# Summer Studio Repository

## Useful bash script
If you try run the ```notebook.sh``` it should run the Jupyter Lab

```bash
bash notebook.sh
```

If you try run the ```run.sh``` it will allow you to run the Docker container's bash terminal.

```bash
bash run.sh
```
