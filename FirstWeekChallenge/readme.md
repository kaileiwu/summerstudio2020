# Instructions for Challenge
- You are to design and train a neural network that is able to identify the item in an image from the CIFAR10 dataset
- The training inputs are seperated per item type
- Below lists the original dataset's format and how they are represented.

# Rules
- You will not need to make a testing set, but it is up to you if you want to use a validation set. 
- You will need to upload your completed model notebook to your personal gitlab forks.
- You will be limited to using the concepts we covered in first week (You cannot use CNN layers)


### Notes from the Dataset Authors

1. Title: The CIFAR-10 dataset

2. Source Information

a) Creators:

	Alex Krizhevsky, Vinod Nair, and Geoffrey Hinton.

c) Date of release: 2009
 
3. Description:

    1. The CIFAR-10 dataset consists of 60000 32x32 colour images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images.

    2. The dataset is divided into five training batches and one test batch, each with 10000 images. The test batch contains exactly 1000 randomly-selected images from each class. The training batches contain the remaining images in random order, but some training batches may contain more images from one class than another. Between them, the training batches contain exactly 5000 images from each class.

4. List of Classes:
   1. airplane
   2. automobile
   3. bird
   4. cat
   5. deer
   6. dog
   7. frog
   8. horse
   9. ship
   10. truck

5. Relevant Information:

   1.  The classes are completely mutually exclusive. There is no overlap between automobiles and trucks. "Automobile" includes sedans, SUVs, things of that sort. "Truck" includes only big trucks. Neither includes pickup trucks.

